// // import firebase from 'firebase';
// import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import 'rxjs/add/operator/map';
// import { Injectable } from '@angular/core';
// @Injectable()
// export class AuthService {
//     constructor(private _http: Http) { }
//     private _loginApi = 'http://139.59.34.101:8002/authenticate';
//     // signup(email: string, password: string) {
//     //     return firebase.auth().createUserWithEmailAndPassword(email, password);
//     // }
//     // signin(email: string, password: string) {
//     //     return firebase.auth().signInWithEmailAndPassword(email, password);
//     // }
//     signin(email: string, password: string) {
//         let headers = new Headers(
//             {
//                 'Content-Type': 'application/json'
//             });
//         let options = new RequestOptions({ headers: headers });

//         let data = JSON.stringify({
//             "Email": email,
//             "Password": password
//         });

//         return this._http.post(this._loginApi, data, options)
//       .map((response: Response) => response.json());
//     }
//     // logout() {
//     //     firebase.auth().signOut();
//     // }
// }
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

    private _loginApi = 'http://139.59.34.101:8002/authenticate';
    private _signUpApi = 'http://139.59.34.101:8002/createNewUser';

    constructor(private _http: Http) { }

    postLogin(email: string, password: string) {
        const data = {
            "Email": email,
            "Password": password
        }
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.post(this._loginApi, JSON.stringify(data), options)
            .map((response: Response) => response.json());
    }
    signUp(postdata: any) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this._http.post(this._signUpApi, JSON.stringify(postdata), options)
            .map((response: Response) => response.json());
    }
    logout() {
        //firebase.auth().signOut();
    }
}
