import { FeedbackuserdetailsPage } from './../feedbackuserdetails/feedbackuserdetails';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FeedbackprogramdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedbackprogramdetails',
  templateUrl: 'feedbackprogramdetails.html',
})
export class FeedbackprogramdetailsPage {
  feedbackuserdetails=FeedbackuserdetailsPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackprogramdetailsPage');
  }
  onLoad(page: any) {
    this.navCtrl.push(page)
    .catch((error)=> console.log('Access Denied',error))
  }
}
