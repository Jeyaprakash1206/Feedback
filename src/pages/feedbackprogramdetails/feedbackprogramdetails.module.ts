import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackprogramdetailsPage } from './feedbackprogramdetails';

@NgModule({
  declarations: [
    FeedbackprogramdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackprogramdetailsPage),
  ],
})
export class FeedbackprogramdetailsPageModule {}
