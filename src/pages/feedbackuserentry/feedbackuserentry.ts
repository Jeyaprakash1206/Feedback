import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeedbackprogramdetailsPage } from '../feedbackprogramdetails/feedbackprogramdetails';

/**
 * Generated class for the FeedbackuserentryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedbackuserentry',
  templateUrl: 'feedbackuserentry.html',
})
export class FeedbackuserentryPage {
  feedbackprogramdetails=FeedbackprogramdetailsPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackuserentryPage');
  }
  onLoad(page: any) {
    this.navCtrl.push(page)
    .catch((error)=> console.log('Access Denied',error))
  }
}
