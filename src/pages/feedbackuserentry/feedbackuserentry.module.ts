import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackuserentryPage } from './feedbackuserentry';

@NgModule({
  declarations: [
    FeedbackuserentryPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackuserentryPage),
  ],
})
export class FeedbackuserentryPageModule {}
