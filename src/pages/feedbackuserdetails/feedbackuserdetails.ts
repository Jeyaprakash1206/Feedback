import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FeedbackformPage } from '../feedbackform/feedbackform';

/**
 * Generated class for the FeedbackuserdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedbackuserdetails',
  templateUrl: 'feedbackuserdetails.html',
})
export class FeedbackuserdetailsPage {
  feedbackform=FeedbackformPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackuserdetailsPage');
  }
  onLoad(page: any) {
    this.navCtrl.push(page)
    .catch((error)=> console.log('Access Denied',error))
  }
}
