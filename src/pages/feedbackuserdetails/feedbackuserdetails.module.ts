import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackuserdetailsPage } from './feedbackuserdetails';

@NgModule({
  declarations: [
    FeedbackuserdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackuserdetailsPage),
  ],
})
export class FeedbackuserdetailsPageModule {}
