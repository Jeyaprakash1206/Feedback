import { ReportPage } from './../report/report';
import { MyTrainingsPage } from './../my-trainings/my-trainings';
import { MyattendancePage } from './../myattendance/myattendance';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IonicPage, NavParams } from 'ionic-angular';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myattendancePage = MyattendancePage;
  myTrainingsPage = MyTrainingsPage;
  reportPage=ReportPage;
  constructor(public navCtrl: NavController) {

  }
  onLoad(page: any) {
    console.log('page', page);
    this.navCtrl.push(page)
      .catch((error) => console.log('Access Denied', error))
  }

}
