import { HomePage } from './../home/home';
import { FeedbackuserentryPage } from './../feedbackuserentry/feedbackuserentry';
import { SignupPage } from './../signup/signup';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AuthService } from './../../services/auth';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {
  signupPage = SignupPage;
  feedbackuserentry = FeedbackuserentryPage;
  homepage = HomePage;
  constructor(private authService: AuthService, private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) { }
  onSignin(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Signing you in...'
    })
     loading.present();
    this.authService.postLogin(form.value.email, form.value.password)
      .subscribe((response) => {
        loading.dismiss();
        this.onLoad(this.homepage);
      }, (error) => {
        loading.dismiss();
          const alert= this.alertCtrl.create({
           title: 'Signin failed',
           message: JSON.parse(error._body).message,
           buttons: ['Ok'] 
          })
          alert.present();
      });
  }
  onLoad(page: any) {
    this.navCtrl.push(page)
      .catch((error) => console.log('Access Denied', error))
  }

}
