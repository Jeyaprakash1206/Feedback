import { AddTrainingPage } from './../add-training/add-training';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MyTrainingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-trainings',
  templateUrl: 'my-trainings.html',
})
export class MyTrainingsPage {
  addtraining = AddTrainingPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyTrainingsPage');
  }
  onLoad(page: any) {
    console.log('page', page);
    this.navCtrl.push(page)
      .catch((error) => console.log('Access Denied', error))
  }

}
