import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyTrainingsPage } from './my-trainings';

@NgModule({
  declarations: [
    MyTrainingsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyTrainingsPage),
  ],
})
export class MyTrainingsPageModule {}
