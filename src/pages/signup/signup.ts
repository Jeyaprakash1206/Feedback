import { HomePage } from './../home/home';
import { AuthService } from './../../services/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  providers: [AuthService]
})
export class SignupPage {
  home = HomePage;
  constructor(private authService: AuthService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private navCtrl: NavController) {

  }
  onSignup(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Signing you up...'
    })
    loading.present();
    let postData = {
      "FirstName": form.value.firstname,
      "LastName": form.value.lastname,
      "Email": form.value.email,
      "PhoneNumber": form.value.phonenumber,
      "Password": form.value.password,
      "Profession": form.value.Profession,
      "Company": form.value.Company,
      "EmpID": form.value.EmpID,
      "Role": form.value.Role
    }
    this.authService.signUp(postData)
      .subscribe((response) => {
        loading.dismiss();
        if (response.user) {
          this.onLoad(this.home);
        } else {
          const alert = this.alertCtrl.create({
            title: 'Signup failed',
            message: response.message,
            buttons: ['Ok']
          })
          alert.present();
        }
      }, (error) => {
        loading.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Signup failed',
          message: error.message,
          buttons: ['Ok']
        })
        alert.present();
      })
  }
  onLoad(page: any) {
    this.navCtrl.push(page)
      .catch((error) => console.log('Access Denied', error))
  }

}
