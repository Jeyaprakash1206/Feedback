import { ThankyouPage } from './../thankyou/thankyou';
import { ReportPage } from './../report/report';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FeedbackformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedbackform',
  templateUrl: 'feedbackform.html',
})
export class FeedbackformPage {
  reportPage = ReportPage;
  thankyou = ThankyouPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackformPage');
  }
  onModelChange(event){
    console.log('value',event)
  }
  onLoad(page: any) {
    console.log('page', page);
    this.navCtrl.push(page)
      .catch((error) => console.log('Access Denied', error))
  }
}
