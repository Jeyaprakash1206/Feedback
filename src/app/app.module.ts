import { AddTrainingPage } from './../pages/add-training/add-training';
import { ThankyouPage } from './../pages/thankyou/thankyou';
import { ReportPage } from './../pages/report/report';
import { MyTrainingsPage } from './../pages/my-trainings/my-trainings';
import { MyattendancePage } from './../pages/myattendance/myattendance';
import { FeedbackuserentryPage } from './../pages/feedbackuserentry/feedbackuserentry';
import { AuthService } from './../services/auth';
import { SigninPage } from './../pages/signin/signin';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { FeedbackPage } from '../pages/feedback/feedback';
import { FeedbackprogramdetailsPage } from '../pages/feedbackprogramdetails/feedbackprogramdetails';
import { FeedbackuserdetailsPage } from '../pages/feedbackuserdetails/feedbackuserdetails';
import { FeedbackformPage } from '../pages/feedbackform/feedbackform';
import { Ionic2RatingModule } from 'ionic2-rating';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SigninPage,
    SignupPage,
    FeedbackPage,
    FeedbackuserentryPage,
    FeedbackprogramdetailsPage,
    FeedbackuserdetailsPage,
    FeedbackformPage,
    MyattendancePage,
    MyTrainingsPage,
    ReportPage,
    ThankyouPage,
    AddTrainingPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    Ionic2RatingModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,SigninPage,
    SignupPage,FeedbackPage,
    FeedbackuserentryPage,FeedbackprogramdetailsPage,
    FeedbackuserdetailsPage,
    FeedbackformPage,
    MyattendancePage,
    MyTrainingsPage,
    ReportPage,
    ThankyouPage,
    AddTrainingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
