import { AuthService } from './../services/auth';
import {ViewChild } from '@angular/core';
import { SigninPage } from './../pages/signin/signin';
import { Component } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { FeedbackPage } from '../pages/feedback/feedback';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  signinPage = SigninPage;
  signupPage= SignupPage;
  feedback=FeedbackPage;
  isAuthenticated =false;
  @ViewChild('nav') nav: NavController;
  constructor(platform: Platform,private menuCtrl:MenuController,private authService : AuthService, statusBar: StatusBar, splashScreen: SplashScreen) {
    firebase.initializeApp({
      apiKey: "AIzaSyDLmMULlrqy39XPD7ZlqIU83nd81NPrdfI",
    authDomain: "feedback-f0d05.firebaseapp.com"
    });
    firebase.auth().onAuthStateChanged(user =>{
      if(user){
        this.isAuthenticated = true;
        console.log('isAuthenticated',this.isAuthenticated)
        this.rootPage=HomePage;
      }else{
        this.isAuthenticated = false;
        console.log('isAuthenticated',this.isAuthenticated)
        this.rootPage=SigninPage;
      }
    })
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  onLoad(page: any) {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  onLogout(){
    this.authService.logout();
    this.menuCtrl.close();
    this.nav.setRoot(SigninPage);
  }
}

